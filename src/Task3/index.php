<?php

require __DIR__ . '/../../vendor/autoload.php';

use App\Task3\AddData;

$data = new AddData();
$presenter = $data->presenter();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Built-in Web Server</title>
</head>
<body>

    <table>
        <tr>
            <td>
                Name
            </td>
            <td>
                Image
            </td>
        </tr>
        <?=$presenter;?>
    </table>
</body>
</html>
