<?php
declare(strict_types = 1);

namespace App\Task3;

use App\Task1\FightArena;
use App\Task1\Fighter;

class AddData {
    private $fighter1;
    private $fighter2;
    private $fighter3;


    /**
     * @return \App\Task1\Fighter
     */
    private function addFighter1() : Fighter
    {
        return $this->fighter1 = new Fighter(1,
                                   'Ryu',
                                   100,
                                   10,
                                   'https://bit.ly/2E5Pouh');

    }
    private function addFighter2(): Fighter
    {
        return $this->fighter2 = new Fighter(2,
                                   'Chun-Li',
                                   70,
                                   30,
                                   'https://bit.ly/2Vie3lf');
    }

    private function addFighter3(): Fighter
    {
        return $this->fighter3 = new Fighter(3,
                                      'Ken Masters',
                                      80,
                                      20,
                                      'https://bit.ly/2VZ2tQd');
    }
    private function addArenaFighter(): FightArena
    {
        $arena = new FightArena();
        $arena->add($this->addFighter1());
        $arena->add($this->addFighter2());
        $arena->add($this->addFighter3());

        return $arena;

    }

    public function presenter(): string
    {
        $presenter = new FightArenaHtmlPresenter();
        $presentation = $presenter->present($this->addArenaFighter());
        return $presentation;
    }
}