<?php

declare(strict_types = 1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {

        $a = '';

        foreach ($arena->all() as $value){
          $a .= '<tr>
<td>'.$value->getName().': '.$value->getHealth().', '.$value->getAttack().'
</td>
<td>
<img src="'.$value->getImage().'">
</td>
</tr>';

        }

        return $a ;
    }

}
