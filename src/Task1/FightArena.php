<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    private $arrFightArena = [];

    public function add(Fighter $fighter): void
    {
        $this->arrFightArena[] = $fighter;
    }

    public function mostPowerful(): ?Fighter
    {
        $arr = $this->arrFightArena;
        usort($arr, function ($a, $b) { return $b->getAttack() <=> $a->getAttack(); });
        return $arr[0];
    }

    public function mostHealthy(): ?Fighter
    {
        $arr = $this->arrFightArena;
        usort($arr, function ($a, $b) { return $a->getAttack() <=> $b->getAttack(); });
        return $arr[0];

    }

    public function all(): array
    {
        return $this->arrFightArena;
    }




}
