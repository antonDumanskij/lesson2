<?php

declare(strict_types = 1);

namespace App\Task1;

class Fighter
{
    private $arrFighter = [];
    private $id;
    private $name;
    private $health;
    private $attack;
    private $image;

    public function __construct(int $id,
                                string $name,
                                int $health,
                                int $attack,
                                string $image)
    {
        $this->id = $id;
        $this->name = $name;
        $this->health = $health;
        $this->attack = $attack;
        $this->image = $image;

        $this->addArrFighter($this->id,
                             $this->name,
                             $this->health,
                             $this->attack,
                             $this->image);
    }


    private function addArrFighter(int $id,
                                   string $name,
                                   int $health,
                                   int $attack,
                                   string $image): array
    {
        return $this->arrFighter = [
            'id' => $id,
            'name' => $name,
            'health' => $health,
            'attack' => $attack,
            'image' =>$image
        ];


    }

    public function getId(): int
    {
        return $this->arrFighter['id'];
    }

    public function getName(): string
    {
        return $this->arrFighter['name'];
    }

    public function getHealth(): int
    {
        return $this->arrFighter['health'];
    }

    public function getAttack(): int
    {
        return $this->arrFighter['attack'];
    }

    public function getImage(): string
    {
        return $this->arrFighter['image'];
    }
}
