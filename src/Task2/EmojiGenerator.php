<?php

declare(strict_types=1);

namespace App\Task2;

class EmojiGenerator
{
    private $arr = ['🚀', '🚃', '🚄', '🚅', '🚇'];


    public function generate(): \Generator
    {
        yield from $this->arr;
    }
}
